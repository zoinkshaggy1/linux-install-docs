# Generate gpg key with gpg --full-generate-key
# List key with gpg --list-secret-keys --keyid-format LONG
Current key is:  BAE987ECE01C8DB6

# Export
gpg --export-secret-keys zoinkshaggy@gmail.com > private.key

# Import
gpg --import private.key
