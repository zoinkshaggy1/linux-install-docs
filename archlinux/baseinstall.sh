#!/usr/bin/env bash
cp ./pacman.conf /etc
pacman -Syy
reflector --verbose -c US -a 12 -l 10 --sort rate --save /etc/pacman.d/mirrorlist 
timedatectl set-ntp true
pacstrap /mnt base base-devel linux-zen linux-zen-headers linux-firmware amd-ucode os-prober fish rsync reflector git pigz pbzip2 mold
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
