#!/usr/bin/env bash
cp ./makepkg.conf ./pacman.conf /etc
cp ./rust.conf /etc/makepkg.conf.d/
reflector --verbose -c US -a 12 -l 10 --sort rate --save /etc/pacman.d/mirrorlist 
ln -sf /usr/share/zoneinfo/America/Denver /etc/localtime
hwclock --systohc
sed -i '171s/^.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1  localhost" >> /etc/hosts
echo "::1  localhost" >> /etc/hosts
echo "127.0.1.1  arch.localdomain  arch"
systemctl enable {NetworkManager,reflector.timer}
passwd root
grub-install --target=x86_64-efi --efi-directory=/boot
grub-mkconfig -o /boot/grub/grub.cfg
useradd -m -G wheel -s /usr/bin/fish zoinkshaggy
passwd zoinkshaggy
echo "zoinkshaggy ALL=(ALL:ALL) ALL" > /etc/sudoers.d/zoinkshaggy
