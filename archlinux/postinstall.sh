#!/usr/bin/env bash
# Install yay aur helper
sudo pacman -S --needed git base-devel
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

# clone git repos
git clone https://gitlab.com/zoinkshaggy1/dotfiles ~/dotfiles
git clone https://gitlab.com/zoinkshaggy/pass ~/.password-store

# Setup git
git config --global user.email "zoinkshaggy@gmail.com"
git config --global user.name "Chad Petersen"

# Remove fish folder in .config
rm -rvf ~/.config/fish

# Set links for applications
ln -s ~/dotfiles/MangoHud ~/.config/
ln -s ~/dotfiles/avizo ~/.config/
ln -s ~/dotfiles/fish ~/.config/
ln -s ~/dotfiles/helix ~/.config/
ln -s ~/dotfiles/hypr ~/.config/
ln -s ~/dotfiles/kitty ~/.config/
ln -s ~/dotfiles/mc ~/.config/
ln -s ~/dotfiles/mpd ~/.config/
ln -s ~/dotfiles/ncmpcpp ~/.config/
ln -s ~/dotfiles/nwg ~/.config/
ln -s ~/dotfiles/waybar ~/.config/
ln -s ~/dotfiles/wofi-bar ~/.config/

# Install pkgs
yay -S (cat pkgs)

# Services
systemctl enable lactd
systemctl enable bluetooth
systemctl enable sddm

# Mangohud
sudo su -c "echo 'MANGOHUD=1' >> /etc/environment"

# GPG stuff
gpg --import ~/.password-store/private.gpg
for fpr in $(gpg --list-keys --with-colons  | awk -F: '/fpr:/ {print $10}' | sort -u); do  echo -e "5\ny\n" |  gpg --command-fd 0 --expert --edit-key $fpr trust; done

# Set Default Applications
xdg-settings set default-web-browser com.brave.Browser.desktop
xdg-mime default thunar.desktop inode/directory

